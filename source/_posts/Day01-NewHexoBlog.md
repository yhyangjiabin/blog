---
title: Day01-使用Hexo搭建博客
date: 2019-03-25 23:11:22
tags:
---


## 01-Hexo部分

### 01-1-安装Node.js环境

* 下载地址:https://nodejs.org/en/

> 选择LTS版本

![](https://i.loli.net/2019/03/25/5c97c4995a5d4.png)

安装一路下一步就好,我是修改了一下安装路径

![](https://i.loli.net/2019/03/25/5c97c4b508152.png)


安装完成

* 测试是否安装成功

* 测试

在命令提示符下输入命令

{% code lang:bash %}
$ node -v
$ npm -v
{% endcode %}

{% note success %}
会显示当前node的版本:v10.15.3
会显示当前npm的版本:6.4.1
{% endnote %}

### 01-2-安装Hexo-Cli和Hexo

* 安装Hexo-Cli和Hexo

官方网址:https://hexo.io/

在Powershell执行下面命令

{% code lang:bash %}
$ npm install hexo-cli -g 
{% endcode %}

{% note success %}
正在安装:
![](https://i.loli.net/2019/03/25/5c97c4d5d5bc4.png)

安装成功:
![](https://i.loli.net/2019/03/25/5c97c4e75faa1.png)
{% endnote %}

初始化Blog,执行下面命令,也会安装Hexo博客框架

{% code lang:bash %}
$ hexo init blog 
{% endcode %}

{% note success %}
正在初始化Blog:
![](https://i.loli.net/2019/03/25/5c97c6bc311e0.png)

初始化Blog完成:
![](https://i.loli.net/2019/03/25/5c97c6cff2d87.png)
{% endnote %}

测试一下,博客是否可以正常运行

{% code lang:bash %}
$ cd blog # 进入到博客的主目录
$ hexo g | hexo s 
$ 允许网络访问
{% endcode %}

![](https://i.loli.net/2019/03/25/5c97c816ddcec.png)

{% note success %}
博客运行成功:
![](https://i.loli.net/2019/03/25/5c97c82ae1307.png)

在浏览器中输入`http://localhost:4000`,可以看到下面页面显示:
![](https://i.loli.net/2019/03/25/5c97c845d7ada.png)

Ctrl + C 结束Blog运行
{% endnote %}

* 修改主题

主题网站:https://hexo.io/themes/

我选用的Next主题,官网网址:https://theme-next.org/

下载主题文件,在博客的主目录下执行下面命令:

{% code lang:bash %}
$ cd blog
$ git clone https://github.com/theme-next/hexo-theme-next themes/next
$ 删除原来的主题文件
{% endcode %}

{% note info %}
把next主题的Git信息删除,方便后面我们提交代码
删除下面文件和文件夹
.git/
.github/
.gitattributes
.gitignore
{% endnote %}

在next主题目录下,删除上面所说的文件和文件夹

{% code lang:bash %}
$ cd blog/themes/next
$ del .git/ .github/ .gitattributes .gitignore
$ 选择A,删除全部
{% endcode %}

修改`_config.yml`配置文件

{% code lang:yml %}
title: Richard Blog
author: Richard M Hart
language: zh-CN
timezone: UTC

url: http://yhyangjiabin.tech

theme: next
{% endcode %}

附录:小工具

* 数学公式:https://www.mathcha.io/?ref=appinn

用下面固定格式添加数学公式:

{% code lang:html %}
{% raw %}
<p>
When $a \ne 0$, there are two solutions to \(ax^2 + bx + c = 0\) and they are
$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$
</p>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML"></script>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
{% endraw %}
{% endcode %}



* 坐标图:https://www.desmos.com/calculator

* 截图:http://zh.snipaste.com/
* 截图工具教程:https://sspai.com/post/34962

* 图床:https://molunerfinn.com/PicGo/

## 02-GitLab部分

### 02-1-Git本地配置

官网:https://git-scm.com/

前面使用scoop安装过git和openssh了

接下来我们直接配置Git

{% code lang:bash %}
$ git config --global user.name "YHyangjiabin"
$ git config --global user.email "yangjiabin850411@hotmail.com"
{% endcode %}

### 02-2-GitLab仓库和本地配置

官网:https://gitlab.com/

在gitlab网站上,新建项目:blog-temp



gitlab设置SSH key

{% code lang:bash %}
$ ssh-keygen -o -t rsa -b 4096 -C "yangjiabin850411@hotmail.com"
$ cat ~/.ssh/id_rsa.pub | clip
$ ssh -T git@gitlab.com
$ 输入yes
{% endcode %}

{% note success %}
ssh连接成功:

{% endnote %}


把blog初始化,并push到gitlab

{% code lang:bash %}
$ cd blog
$ git init
$ git remote add origin git@gitlab.com:yhyangjiabin/blog-temp.git
$ git add .
$ git commit -m "Initial blog-temp"
$ git push -u origin master
{% endcode %}


## 03-netlify部分

官网:https://app.netlify.com

使用gitlab的账号登陆netlify托管平台

Create a new site

Continuous Deployment 选择 GitLab

直接点击 Deploy site

生成链接:https://festive-heyrovsky-6c5fb5.netlify.com

